from random import randint


def create_greeting():
    user_name = input("Hi! What is your name? ")
    print(f"\nHi {user_name}! Welcome to the Guess Your Birthday Game.")
    print("\nThe game where I try to guess your birth month and year.")
    num_guesses = int(input("\nHow many guesses would you like to give me? "))
    return user_name, num_guesses


def main():
    user_name, num_guesses = create_greeting()

    year_low = 1924
    year_high = 2004
    month_low = 1
    month_high = 12

    for guess_number in range(num_guesses):
        year = randint(year_low, year_high)
        month = randint(month_low, month_high)

        print(
            f"\nGuess {guess_number + 1} out of {num_guesses}: {user_name} were you born in {month} / {year}?\n"
        )
        guess = input("yes or no? (y, n) ")

        if guess.lower() == "exit":
            break

        if guess.lower() == "yes" or guess.lower() == "y":
            print("I knew it! I win!")
            break
        elif guess_number < (num_guesses - 1):
            print("Drat! Lemme try again!")
            month_response = input(
                "\nWas my month guess equal, higher, or lower than your birth month? (e, h, l) "
            )
            if month_response.lower() == "equal" or month_response.lower() == "e":
                month_low = month
                month_high = month
            elif month_response.lower() == "lower" or month_response.lower() == "l":
                month_low = month + 1
            elif month_response.lower() == "higher" or month_response.lower() == "h":
                month_high = month - 1

            year_response = input(
                "\nWas my year guess equal, higher, or lower than your birth month? (e, h, l) "
            )
            if year_response.lower() == "equal" or year_response.lower() == "e":
                year_low = year
                year_high = year
            elif year_response.lower() == "lower" or year_response.lower() == "l":
                year_low = year + 1
            elif year_response.lower() == "higher" or year_response.lower() == "h":
                year_high = year - 1

        else:
            print("\nI have other things to do.")

    print("Goodbye.")


if __name__ == "__main__":
    main()
